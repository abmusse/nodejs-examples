# Tutorial 2: Connecting to DB2

One of the most important tasks of any web server is to communicate with a back-end data store, most commonly a database. Connecting to DB2 is made easier through packages such as idb-pconnector, which can be found on npm. To install it, navigate to your projects directory and run:

```text
npm install idb-pconnector --save
```

From there we can require idb-pconnector and use it within our application. Documentation for the idb-pconnector and examples can be found at the [repository](https://bitbucket.org/litmis/nodejs-idb-pconnector).  For this tutorial we will make use of the connection pooling functionality built into idp-connector package. We can require the pool with

```javascript
//Express
const express = require('express');
const app = express();
const SCHEMA = 'BOOKSTORE';
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});
const port = process.env.PORT || 3001;


//View Engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use('/assets', express.static(__dirname+'/public'));

//DB2 Driver
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true});
```

Within the DBPool constructor As the first parameter we can pass  **database**: object which contains the `url`\(database name\).`username`, and `password.` If a database name is not provided defaults to \*LOCAL, `username` and `password` assumed blank if not specified with non-local URL.

As the second parameter we can pass a  **config**: `object` , which contains the `incrementSize` and `debug`. Increment size sets the desired size of the DB Pool. If none specified, defaults to 8 connections. Setting debug = true will display message logs.

Now that we can connect to DB2 we need to create a table that will store our books. Within the project tree this code is found within `db.js` file. We only need to run this code once during initial setup.

```javascript
async function createBooksTable(){
  try {
    let sql = `CREATE OR REPLACE TABLE
                 BOOKSTORE.Books(bookId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
                 title VARCHAR(30) NOT NULL,
                 isbn VARCHAR(20) NOT NULL,
                 amount DECIMAL(10 , 2) NOT NULL, PRIMARY KEY (bookId))`;
    pool.runSql(sql);
  } catch (err) {
    console.log(`Error Creating Table:\n${err.stack}`);
  }
};
createBooksTable();
```