function QSYSGETPH(user, pw, cb){
  let xt = require('itoolkit'),
    conn = new xt.iConn('*LOCAL'),
    pgm = new xt.iPgm('QSYGETPH', {lib:'QSYS', error:'off'});

  pgm.addParam(user.toUpperCase(), '10A');
  pgm.addParam(pw.toUpperCase(), '10A');
  pgm.addParam('', '12A', {io:'out', hex:'on'});
  let errno = [
    [0, '10i0'],
    [0, '10i0', {setlen:'rec2'}],
    ['', '7A'],
    ['', '1A']
  ];
  pgm.addParam(errno, {io:'both', len : 'rec2'});
  pgm.addParam(10, '10i0');
  pgm.addParam(0, '10i0');
  conn.add(pgm.toXML());
  conn.run(function(str) {
    let results = xt.xmlToJson(str);
    cb(null, results[0].success);
  }, true);
};

module.exports = QSYSGETPH;
