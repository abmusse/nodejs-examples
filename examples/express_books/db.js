/*
  Database setup for the project
  Run this first or use ACS - Run SQL Scripts
*/
async function createBooksTable(){
  const {DBPool} = require('idb-pconnector');
  const pool = new DBPool({}, {debug:true , incrementSize: 3});
  
  try {
    let sql = `CREATE OR REPLACE TABLE
                 BOOKSTORE.Books(bookId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
                 title VARCHAR(30) NOT NULL,
                 isbn VARCHAR(20) NOT NULL,
                 amount DECIMAL(10 , 2) NOT NULL, PRIMARY KEY (bookId))`;
    pool.runSql(sql);
  } catch (err) {
    console.log(`Error Creating Table:\n${err.stack}`);
  }
};

exports.createBooksTable = createBooksTable;
