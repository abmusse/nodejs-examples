/*
  Date: 8-13-18
  Description: Main Entry Server for express_books example.
  Purpose: Create a Restful API with authentication using passport.js
*/

//Express
const express = require('express');
const app = express();
const port = process.env.PORT || 3001;

//View Engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use('/assets', express.static(__dirname+'/public'));

//Authentication
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const ibmiAuth = require('./ibmiAuth');
const flash = require('connect-flash');

//Register middleware
app.use(session({
  //Secret is unique key to sign the cookie should be random long sequence of chars.
  secret: process.env.SECRET,
  resave: false,
  //if savUninitialized is set to true
  //Cookie will be given to user even if is not logged in, make sure its false.
  saveUninitialized: false,
  //expires in 30 min. 30 min = 1800000 ms
  cookie: { maxAge  : new Date(Date.now() + 1800000) }
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//local strategy is called when passport.authenticate is called. Ex: post req for /login.
passport.use(new LocalStrategy(
  function(username, password, done) {
    ibmiAuth(username, password, (err, result) =>{
      if (result){
        return done(null, username);
      }
      return done(null, false , 'Invalid User Name or Password');
    });
  }
));

passport.serializeUser(function(username, done) {
  done(null, username);
});

passport.deserializeUser(function(username, done) {
  done(null, username);
});

//To be used by routes needing to authenticate
exports.passport = passport;

//middleware function so that views can check if authenticated with locals
app.use(function(req, res, next){
  res.locals.isAuthenticated = req.isAuthenticated();
  next();
});

//Register Routers
const addBookRouter = require('./routes/addBook');
app.use('/addbook', addBookRouter);

const deleteBookRouter = require('./routes/deleteBook');
app.use('/deletebook', deleteBookRouter);

const editRouter = require('./routes/edit');
app.use('/edit', editRouter);

const getBookRouter = require('./routes/getBook');
app.use('/getbook', getBookRouter);

const indexRouter = require('./routes/index');
app.use('/', indexRouter);

const loginRouter = require('./routes/login');

app.use('/login', loginRouter);

const logoutRouter = require('./routes/logout');
app.use('/logout', logoutRouter);

const updateBookRouter = require('./routes/updateBook');
app.use('/updatebook', updateBookRouter);

//display 404 for any mismatch routes
app.use(function (req, res, next) {
  let options = {
    root: __dirname + '/public/html'
  }
  res.sendFile('404.html', options);
});

app.listen(port, function(){
  console.log(`Bookstore listening @ localhost:${port}`);
});
