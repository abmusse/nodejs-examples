/*
  middleware function to check if authenticated 
  before allowing access to secure routes
  req.isAuthenticated is provided by Passport.js
  Passport should be initialized and setup before use
*/
function checkLogin(){
    return (req, res, next) =>{
      if (req.isAuthenticated()){
        console.log(`\n[Session Info]: ${JSON.stringify(req.session)}\n`);
        return next();
      }
      res.render('login', {error: 'Must Login to Access Page'});
    };
  }
module.exports = checkLogin;