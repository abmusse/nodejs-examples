const router = require('express').Router();
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true , incrementSize: 3});
const SCHEMA = 'BOOKSTORE';

router.get('/:id', async (req, res) =>{
    try {
      let sql = `SELECT * FROM ${SCHEMA}.BOOKS WHERE bookid = ?`,
        results;
  
      results = await pool.prepareExecute(sql, [req.params.id]);
  
      if (results !== null){
        //return the book as json
        res.json(results);
      } else {
        res.send('no results');
      }
    }   catch (err){
      console.log(`Error SELECTING BY Book id:\n${err.stack}`);
    }
  
  });

  module.exports = router;