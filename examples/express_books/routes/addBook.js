const router = require('express').Router();
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true , incrementSize: 3});
const SCHEMA = 'BOOKSTORE';
const checkLogin = require('../checkLogin');
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});

//register middleware
router.use(urlencodedParser);
router.use(checkLogin());
//add a new book
router.post('/', async (req, res) =>{
    console.log(`${req.body.title} ${req.body.isbn} ${req.body.amount}`);
    //TODO validate form inputs
    let title = req.body.title,
      isbn = parseInt(req.body.isbn),
      amount = parseFloat(req.body.amount).toFixed(2);
  
      //add book to the DB
    try {
      let sql = `INSERT INTO ${SCHEMA}.BOOKS(title, isbn, amount) VALUES (?, ?, ?)`,
        results = null,
        book = {};
  
      results = await pool.prepareExecute(sql, [title, isbn, amount]);
      res.send(true);
    }   catch (err){
      res.send(false);
      console.log(`Error ADDING NEW BOOK:\n${err.stack}`);
    }
  
  });

module.exports = router;