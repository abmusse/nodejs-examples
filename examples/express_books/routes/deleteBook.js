const router = require('express').Router();
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true , incrementSize: 3});
const SCHEMA = 'BOOKSTORE';
const checkLogin = require('../checkLogin');

//register middleware
router.use(checkLogin());

//remove a book by its id
router.delete('/:id', async(req, res) =>{
    try {
      let sql = `DELETE FROM ${SCHEMA}.BOOKS WHERE BOOKID = ${req.params.id}`,
        connection = await pool.attach(),
        affectedRows;
  
      await connection.getStatement().prepare(sql);
      await connection.getStatement().execute();
      affectedRows = await connection.getStatement().numRows();
  
      if (affectedRows > 0){
        res.send(true);
      } else {
        res.send(false);
      }
  
    }   catch (err){
      console.log(`Error DELETING BOOK:\n${err.stack}`);
    }
  });

module.exports = router;