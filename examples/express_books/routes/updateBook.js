const router = require('express').Router();
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true , incrementSize: 3});
const SCHEMA = 'BOOKSTORE';
const checkLogin = require('../checkLogin');
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: false});

//register middleware
router.use(checkLogin());
router.use(urlencodedParser);
//update an exsisting book
router.put('/', async (req, res) =>{
    try {
      let title = req.body.title,
        isbn = parseInt(req.body.isbn),
        amount = parseFloat(req.body.amount).toFixed(2),
        id = parseInt(req.body.id);
  
      let sql = `UPDATE ${SCHEMA}.BOOKS SET TITLE = ?, ISBN = ?, AMOUNT = ?
                 WHERE BOOKID = ?`;
  
      await pool.prepareExecute(sql, [title, isbn, amount, id]);
      res.send(true);
    } catch (err){
      res.send(false);
      console.log(`Error UPDATING BOOK:\n${err.stack}`);
    }
  });

module.exports = router;