const router = require('express').Router();
const {DBPool} = require('idb-pconnector');
const pool = new DBPool({}, {debug:true , incrementSize: 3});
const SCHEMA = 'BOOKSTORE';

//Respond with Static Book Table
router.get('/', async (req, res) =>{
    try {
      let sql = `SELECT * FROM ${SCHEMA}.BOOKS`,
        title = 'ALL BOOKS',
        results;
  
      results = await pool.prepareExecute(sql);
      console.log(results);
      res.render('staticTable.ejs', {title: title, results: results} );
  
    }   catch (err){
      console.log(`Error SELECTING ALL BOOKS:\n${err.stack}`);
    }
  });

module.exports = router;