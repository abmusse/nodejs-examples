const Koa = require('koa');
const Router = require('koa-router');
const dba = require('idb-pconnector');

const app = new Koa();

// setting up our database variables
let dbaConnection = new dba.Connection().connect();
const SCHEMA = "MARK";

async function setupDatabaseTables() {
  // setting up the database tables
  try {
    var dbStatement = dbaConnection.getStatement();
    await dbStatement.exec("CREATE OR REPLACE TABLE " + SCHEMA + ".KOA_USERS(USERNAME VARCHAR(32) NOT NULL, PASSWORD VARCHAR(100) ALLOCATE(20));");
  } catch (dbError) {
    console.error("Error is " + dbError);
  } finally {
    dbStatement.close();
  }
}

// setting up our routes
let appRoutes = new Router();

appRoutes.get('/users', async function(ctx, next) {

  ctx.body = 'Doing database work. Check your terminal for the data!';

  try {
    // create database statements using our idb-pconnector to connect to DB2
    // create a new table, using your own SCHEMA
    var dbStmt =  new dba.Connection().connect().getStatement();
    await dbStmt.exec("CREATE OR REPLACE TABLE " + SCHEMA + ".KOA_USERS(USERNAME VARCHAR(32) NOT NULL, PASSWORD VARCHAR(100) ALLOCATE(20));");
    await dbStmt.prepare("INSERT INTO " + SCHEMA + ".USERS VALUES (?,?)");
    await dbStmt.bind([ ['myuser', dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR], ['mypassword', dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR]]);
    await dbStmt.execute();
    await dbStmt.commit();

    // get the results from our statements and print to the console
    var res = await dbStmt.exec("SELECT * FROM " + SCHEMA + ".USERS");
    console.log("Select results: " + JSON.stringify(res));

  } catch(dbError) {
    console.error("Error is " + dbError);
  } 

  next();
});

// logger
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

app.use(appRoutes.routes());

app.use(async (ctx, next) => {
  try {
    await next()
    if (ctx.status === 404) {
      console.log("404");
    }
  } catch (err) {
    // handle error
  }
});

app.listen(3005);
