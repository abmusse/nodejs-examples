# Tutorial 2: Connecting Your Server to DB2

One of the most important functions of any server is to connect to a data store, usually a database. For the IBM i, this means connecting to the internal DB2 database. Connecting to DB2 is made easier through packages such as idb-pconnector, which can be found on npm. To install it, navigate to your projects directory and run:

`npm install idb-pconnector --save`

With this package installed, you now have an easy way to interface your Node.js app with your internal DB2 database. 

idb-pconnector is the next generation of connectors for DB2 that utilizes pooling for improved performance. The package currently in pre-alpha and should not be used in production environments. For a more mature package \(that doesn't use a pool of connections\), see [idb-connector.](https://www.npmjs.com/package/idb-connector)

We also need to download a package that allows us to define routes for our Koa server. Without routes, any page we hit on our server will go through the same middleware and will print out the same information to the page. Koa is designed to be as minimalist as possible, and out-of-the-box it doesn't include as much of the functionality that a more comprehensive framework like Express is packaged with. To install routing support for Koa, navigate to your projects directory \(where you called `npm init` and where your **packages.json** file lives\) and run:

`npm install koa-router --save`

With these two new packages installed, we can now create a simple application to add users to a database.

```javascript
const Koa = require('koa');
const app = new Koa();

dba = require('idb-pconnector');

const SCHEMA = "YOUR_SCHEMA";

// logger
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

// response
app.use(async ctx => {

  ctx.body = 'Doing database work. Check your terminal for the data!';

  try {
    // create database statements using our idb-pconnector to connect to DB2
    // create a new table, using your own SCHEMA
    var dbStmt =  new dba.Connection().connect().getStatement();
    await dbStmt.exec("CREATE OR REPLACE TABLE " + SCHEMA + ".USERS(USERNAME VARCHAR(32) NOT NULL, PASSWORD VARCHAR(100) ALLOCATE(20));");
    await dbStmt.prepare("INSERT INTO " + SCHEMA + ".USERS VALUES (?,?)");
    await dbStmt.bind([ ['myuser', dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR], ['mypassword', dba.SQL_PARAM_INPUT, dba.SQL_BIND_CHAR]]);
    await dbStmt.execute();
    await dbStmt.commit();

    // get the results from our statements and print to the console
    var res = await dbStmt.exec("SELECT * FROM " + SCHEMA + ".USERS");
    console.log("Select results: " + JSON.stringify(res));

  } catch(dbError) {
    console.error("Error is " + dbError);
  } 
});

app.listen(3000);
```

Be sure to include the name of your own DB2 schema on line 6. 

This simple example again uses the Koa middleware stack to track the time it takes to service a request, but this time the request creates a new database table and inserts some dummy data. It then selects that data to show that it is present in your DB2 database. If you were to check the database on your i System machine, you would see a new table named USERS with two columns, USERNAME and PASSWORD and one entry with the name.

You might notice that we don't select our database or login anywhere in the code. The connector defaults to using the \*LOCAL database, with the credentials of the user currently logged into the i System \(using their credentials\). To log into a specific database, you can pass the database name in the connect\(\) call on line 24, like:


```javascript
var dbStmt =  new dba.Connection().connect("myDB").getStatement();
```