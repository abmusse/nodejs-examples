// app/models/bear.js
// =============================================================================

// BASE SETUP
// =============================================================================

// converted to node 6
// =============================================================================
global._bear_pool_conn_incr_size = 8;
global._bear_database = "*LOCAL";
global._bear_schema = "BEARS";
global._bear_table = "BEAR";
// new improved db2 async
global._bear_db = require('/QOpenSys/QIBM/ProdData/OPS/Node6/os400/db2i/lib/db2a');

// bear response
//
// HTTP Verb  CRUD            Entire Collection (e.g. /customers)           Specific Item (e.g. /customers/{id})
// =========  ==============  ============================================  =============================================
// POST       Create          201 (Created), 'Location' header with link    404 (Not Found).
//                                to /customers/{id} containing new ID.     409 (Conflict) if resource already exists.
// GET        Read            200 (OK), list of customers. Use pagination,  200 (OK), single customer.
//                                sorting and filtering to navigate         404 (Not Found), if ID not found or invalid.
//                                big lists.
// PUT        Update/Replace  404 (Not Found), unless you want to           200 (OK) or 204 (No Content).
//                                 update/replace every resource            404 (Not Found), if ID not found or invalid.
//                                 in the entire collection.           
// PATCH      Update/Modify   404 (Not Found), unless you want to           200 (OK) or 204 (No Content).
//                                modify the collection itself.             404 (Not Found), if ID not found or invalid.
// DELETE     Delete          404 (Not Found), unless you want to           200 (OK).
//                                delete the whole collection               404 (Not Found), if ID not found or invalid.
// =============================================================================
global._bear_msg_10001 = 10001;
global._bear_msg_10001_text = "access failed";
global._bear_msg_10002 = 10002;
global._bear_msg_10002_text = "rows found";
global._bear_msg_10003 = 10003;
global._bear_msg_10003_text = "no rows found";
global._bear_msg_10004 = 10004;
global._bear_msg_10004_text = "insert success, last id"
global._bear_msg_10006 = 10006;
global._bear_msg_10006_text = "row found id";
global._bear_msg_10007 = 10007;
global._bear_msg_10007_text = "no row found id";
global._bear_msg_19999 = 19999;
global._bear_msg_19999_text = "message not available";
global._bear_callback = function(callback, iok, istatus, iresult, imessage, imdata) {
  var msg = "";
  switch (imessage) {
  case global._bear_msg_10001:
    msg = global._bear_msg_10001_text + " (" + global._bear_schema + "." + global._bear_table + ")";
    break;
  case global._bear_msg_10002:
    msg = global._bear_msg_10002_text + " (" + global._bear_schema + "." + global._bear_table + ")";
    break;
  case global._bear_msg_10003:
    msg = global._bear_msg_10003_text + " (" + global._bear_schema + "." + global._bear_table + ")";
    break;
  case global._bear_msg_10004:
    msg = global._bear_msg_10004_text + " (" + imdata + ")";
    break;
  case global._bear_msg_10006:
    msg = global._bear_msg_10006_text + " (" + imdata + ")";
    break;
  case global._bear_msg_10007:
    msg = global._bear_msg_10007_text + " (" + imdata + ")";
    break;
  default:
    msg = global._bear_msg_19999_text + " (" + imessage + ")";
    break;
  }
  var rsp = {
          "ok":iok,
          "status":istatus,
          "message":msg,
          "result":iresult
         };
  callback(rsp);
};
global._bear_callback_200 = function(callback, iresult, imessage, imdata) {
  global._bear_callback(callback, true, 200, iresult, imessage, imdata);
}
global._bear_callback_201 = function(callback, iresult, imessage, imdata) {
  global._bear_callback(callback, true, 201, iresult, imessage, imdata);
}
global._bear_callback_404 = function(callback, iresult, imessage, imdata) {
  global._bear_callback(callback, false, 404, iresult, imessage, imdata);
}

// bear cache (150/hits/second to 2000+/hits/second)
// =============================================================================
global._bear_cache_list = "deadbeef";
global._bear_cache_expire = 3;
BearCache = function() {
  this.cache = {};
  this.t1 = new Date();
  this.expire = function(pthis) {
    // expire cache setting seconds 
    var t2 = new Date();
    var difference = (t2 - pthis.t1) / 1000;
    if (difference > global._bear_cache_expire) {
      pthis.cache = {};
      pthis.t1 = new Date();
    }
  }
}
BearCache.prototype.add = function(key, query) {
  this.cache[key] = query;
}
BearCache.prototype.exist = function(key) {
  this.expire(this);
  if (this.cache[key] == undefined) {
    return false;
  }
  return true;
}
BearCache.prototype.find = function(key) {
  this.expire(this);
  if (this.cache[key] == undefined) {
    return null;
  }
  return this.cache[key];
}
global._bear_cache = new BearCache();

// bear connection
// =============================================================================
BearConn = function(idx) {
  this.conn = new global._bear_db.dbconn();
  this.conn.conn(global._bear_database);
  this.inuse = false;
  this.schema = global._bear_schema;
  this.table = global._bear_table;
  this.idx = idx;
  this.stmt = new global._bear_db.dbstmt(this.conn);
}
BearConn.prototype.free = function() {
  var newstmt = new global._bear_db.dbstmt(this.conn); // different stmt nbr than delete below (debug)
  if (this.stmt) {
    delete this.stmt;
  }
  this.stmt = newstmt;
}
BearConn.prototype.detach = function() {
  var newstmt = new global._bear_db.dbstmt(this.conn); // different stmt nbr than delete below (debug)
  if (this.stmt) {
    delete this.stmt;
  }
  this.stmt = newstmt;
  this.inuse = false;
}
BearConn.prototype.getInUse = function() {
  return this.inuse;
}
BearConn.prototype.setInUse = function() {
  this.inuse = true;
}

// bear connection pool
// =============================================================================
BearPool = function() {
  this.pool = [];
  this.pmax = 0;
}
BearPool.prototype.attach = function(callback) {
  var valid_conn = false;
  while (!valid_conn) {
    // find available connection
    for(var i = 0; i < this.pmax; i++) {
      var inuse = this.pool[i].getInUse();
      if (!inuse) {
        this.pool[i].setInUse();
        callback(this.pool[i]);
        return;
      }
    }
    // expand the connection pool
    var j = this.pmax;
    for(var i = 0; i < global._bear_pool_conn_incr_size; i++) {
      this.pool[j] = new BearConn(j);
      j++;
    }
    this.pmax += global._bear_pool_conn_incr_size;
  }
}

// a bear
// =============================================================================

// Bear ctor
Bear = function() {
  this.bpool = new BearPool();
  // bad news
  this.bearDead = function(pthis) { 
    throw new Error("(" + global._bear_schema + "." + global._bear_table + ")");
  }
  // Is table bears.bear ok?
  this.bearCheck = function (pthis, callback) {
    pthis.bpool.attach( function(bconn) {
      var sql = "SELECT TABLE_OWNER, TABLE_NAME FROM QSYS2.SYSTABLES where TABLE_NAME='" + bconn.table + "'";
      try {
        bconn.stmt.execSync(sql, function (result) {
          bconn.detach();
          if (result[0] == undefined) {
            callback(pthis);
          }
        });
      } catch(ex) {
        bconn.detach();
        callback(pthis);
      }
    });
  }
  // create table bears.bear
  this.bearTable = function (pthis) {
    // Create table bears.bear
    pthis.bpool.attach( function(bconn) {
      var sql = "CREATE TABLE " + bconn.schema + "." + bconn.table 
              + "(id INTEGER GENERATED BY DEFAULT AS IDENTITY (START WITH 1) PRIMARY KEY, name varchar(256) not null)";
      try {
        bconn.stmt.execSync(sql, function (result) {
          bconn.detach();
        });
      } catch(ex) {
        bconn.detach();
      }
    });
    // Is table bears.bear ok?
    pthis.bearCheck(pthis, pthis.bearDead);
  }
  // create table bears.bear
  this.bearSchema = function (pthis) {
    // Create schema
    pthis.bpool.attach( function(bconn) {
      var sql = "CREATE SCHEMA " + bconn.schema;
      try {
        bconn.stmt.execSync(sql, function (result) { 
          bconn.detach(); 
        });
      } catch(ex) {
        bconn.detach();
      }
    });
    // Is table bears.bear ok?
    pthis.bearCheck(pthis, pthis.bearTable);
  }
  // check or create table bears.bear
  this.bearCheck(this, this.bearSchema);
};

// select all rows bears.bear
// Bear.find(function(response){});
// GET        Read            200 (OK), list of customers. Use pagination,  200 (OK), single customer.
//                                sorting and filtering to navigate         404 (Not Found), if ID not found or invalid.
//                                big lists.
Bear.prototype.find = function(callback) {
  var exist = global._bear_cache.exist(global._bear_cache_list);
  if (exist) {
    var found = global._bear_cache.find(global._bear_cache_list);
    if (found) {
      global._bear_callback_200(callback,found,global._bear_msg_10002,null);
    } else {
      global._bear_callback_404(callback,false,global._bear_msg_10003,null);
    }
    return;
  }
  this.bpool.attach( function(bconn) {
    var sql = "select * from " + bconn.schema + "." + bconn.table + " order by name";
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.detach();
        if (query[0] == undefined) {
          global._bear_cache.add(global._bear_cache_list, false);
          global._bear_callback_404(callback,false,global._bear_msg_10003,null);
        } else {
          global._bear_cache.add(global._bear_cache_list, query);
          global._bear_callback_200(callback,query,global._bear_msg_10002,null);
        }
      });
    } catch(ex) {
      bconn.detach();
      global._bear_callback_404(callback,false,global._bear_msg_10001,null);
    }
  });
}

// add row bears.bear
// Bear.save(myname, function(response){});
// POST       Create          201 (Created), 'Location' header with link    404 (Not Found).
//                                to /customers/{id} containing new ID.     409 (Conflict) if resource already exists.
Bear.prototype.save = function(myname, callback) {
  this.bpool.attach( function(bconn) {
    var sql = "insert into " + bconn.schema + "." + bconn.table + " (NAME) VALUES('" + myname +"')";
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.free();
        try {
          sql = "SELECT IDENTITY_VAL_LOCAL() FROM SYSIBM.SYSDUMMY1";
          bconn.stmt.exec(sql, function (query) {
            bconn.detach();
            if (query[0] == undefined) {
              global._bear_callback_201(callback,false,global._bear_msg_10004,"?");
            } else {
              global._bear_callback_201(callback,false,global._bear_msg_10004,query[0]["00001"]);
            }
          });
        } catch(ex) { 
          bconn.detach();
          global._bear_callback_201(callback,false,global._bear_msg_10004,"?");
        }
      });
    } catch(ex) {
      bconn.detach();
      global._bear_callback_404(callback,false,global._bear_msg_10001,null);
    }
  });
}

// select by id row bears.bear
// Bear.findById(myid, function(response){});
// GET        Read            200 (OK), list of customers. Use pagination,  200 (OK), single customer.
//                                sorting and filtering to navigate         404 (Not Found), if ID not found or invalid.
//                                big lists.
Bear.prototype.findById = function(myid, callback) {
  var exist = global._bear_cache.exist(myid);
  if (exist) {
    var found = global._bear_cache.find(myid);
    if (found) {
      global._bear_callback_200(callback,found,global._bear_msg_10006,myid);
    } else {
      global._bear_callback_404(callback,false,global._bear_msg_10007,myid);
    }
    return;
  }
  this.bpool.attach( function(bconn) {
    var sql = "select * from " + bconn.schema + "." + bconn.table + " where ID=" + myid;
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.detach();
        if (query[0] == undefined) {
          global._bear_cache.add(myid, false);
          global._bear_callback_404(callback,false,global._bear_msg_10007,myid);
        } else {
          global._bear_cache.add(myid, query);
          global._bear_callback_200(callback,query,global._bear_msg_10006,myid);
        }
      });
    } catch(ex) {
      bconn.detach();
      global._bear_callback_404(callback,false,global._bear_msg_10001,null);
    }
  });
}

// remove by id row bears.bear
// Bear.removeById(myid, function(response){});
// DELETE     Delete          404 (Not Found), unless you want to           200 (OK).
//                                delete the whole collection               404 (Not Found), if ID not found or invalid.
Bear.prototype.removeById = function(myid, callback) {
  this.bpool.attach( function(bconn) {
    var sql = "select * from " + bconn.schema + "." + bconn.table + " where ID=" + myid;
    try {
      bconn.stmt.exec(sql, function (query) {
        bconn.free();
        if (query[0] == undefined) {
          bconn.detach();
          global._bear_callback_404(callback,false,global._bear_msg_10007,myid);
        } else {
          var sql = "delete from " + bconn.schema + "." + bconn.table + " where ID=" + myid;
          try {
            bconn.stmt.exec(sql, function (query) {
              bconn.detach();
              global._bear_callback_200(callback,false,global._bear_msg_10006,myid);
            });
          } catch(ex) {
            bconn.detach();
            global._bear_callback_404(callback,false,global._bear_msg_10001);
          }
        }
      });
    } catch(ex) {
      bconn.detach();
      global._bear_callback_404(callback,false,global._bear_msg_10001);
    }
  });
}


// debug: expose BearPool
// Bear.debugBearPool(function(pool){});
Bear.prototype.debugBearPool = function(callback) {
  callback(this.bpool);
}


exports.Bear = Bear;
// debug
exports.BearConn = BearConn;
exports.BearPool = BearPool;


