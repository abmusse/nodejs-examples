# node.js tutorial
This tutorial is a simple simulation of a project with customer requirements.

The focus of this project is using node with IBM i provided db2 extension (db2a).

The original project was for node 4. Various tutorial steps, tests, etc., to build the project.
The refactor to node 6 moved all tutoorial 'steps' into alternative source directory, 
leaving 'final' product in zoo demo below. Original tutorial 'steps' were not tested or ported to node 6.

##YIPS bear zoo demo
* [YIPS bear zoo](http://yips.idevcloud.com/zoo/)

## pitch
* [presentation](http://www.semiug.org/2016/docs/nodejs2016.pdf)


# The project
The customer wants one service for both business REST JSON clients (other programs), 
and traditional browser customers.
A simulated project constraint of two different teams was added, where JSON API people deal with the model (db2), and,
art centric web designer people deal with the view (browser). This is common for sites with theme.

```
Project requirements.
> JSON API interface
> Browser interface
> Serve 2000/hits second
> Current LPAR hardware partition (small)
> Isolate Node.js from root of machine
> Integrated into existing Apache site
> Different teams API and GUI (browser)
> Use only JavaScript and html skills
```


## Technical

Based on:  https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4

Download available: https://bitbucket.org/litmis/nodejs/downloads

## Connection pool
This project demonstrates a version of connection pooling technique. Why? 
DB2 rule is never share a connection or statement across threads at same time.
Node.js is not threaded per say, instead uses an event loop main thread to process the 'script'. 
However, long running activities, mostly 'I/O' operations, nodejs extension c code provider convention
is 'run asynchronous' worker thread away from main event loop. The new db2a interfaces are implemented with worker threads.
Therefore, we must use a connection pool to avoid 'sharing a connection' accross threads.

## JSON API and GUI
Most nodejs projects couple view with controller/model using the express, jade/pug. Nothing wrong with this idea. 
However, this project wants to include both browser clients and API JSON REST clients with few steps as possible. A separation of 
human tasks view/model allows for a demonstration of integrating view elements into existing Apache, 
wherein all view elements are handle by Apache (not nodejs). This enables high performance advances like Apache FRCA to be used
independently on view elements with no effect on nodejs model server 
(http://www.ibm.com/support/knowledgecenter/ssw_ibm_i_71/rzaie/rzaieconfrca.htm).

## Cache
A 'live' cache is demonstrated in bear model to achieve customer requirements for 2000/hits a second (millions per hour). 
In this case, reality of the consumer experience is not even noticeably impacted 
by a 3 second memory cache truth. That is to say, add or update of bear names is so rare, probability of a 
cache lie event occurring approaches zero.

## node

```
$ cat package.json 
{
    "name": "node-api",
    "main": "server.js",
    "dependencies": {
        "express": "~4.0.0",
        "body-parser": "~1.0.1"
    }
}

$ npm install
```

Traditionally, start node application by hand (node server.js).
This application was deployed using mama. Please refer to mama project for configuration.

## mama fastcgi (Apache integration).
* [YIPS Apache mama](https://bitbucket.org/litmis/mama)



