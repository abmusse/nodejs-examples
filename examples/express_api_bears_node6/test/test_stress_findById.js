var bear = require('../app/models/bear');

if (process.argv[2] == undefined) {
  console.log("Syntax: test_bear_findById nbr");
  process.exit(-1);
}
GLOBAL._test_nbr = process.argv[2];
GLOBAL._test_making_more = true;
GLOBAL._test_wait_available_count = 0;
GLOBAL._test_wait_available_max = 5;
GLOBAL._test_connections_max_pool = 14;
GLOBAL._test_outgoing_submit = 3;
GLOBAL._test_outgoing_requests = 0;
GLOBAL._test_incoming_response = 0;
GLOBAL._test_outgoing_requests_max = 140;

function checkPool(pond) {
  var pool = pond.pool;
  console.log("=== Pool length %d requests=%d responses=%d ===", pool.length, _test_outgoing_requests, _test_incoming_response);
  if (_test_making_more && (pool.length > _test_connections_max_pool || _test_outgoing_requests > _test_outgoing_requests_max)) {
    if (pool.length > _test_connections_max_pool) {
      console.log("=== Pool length %d beyond max pool, no more submits (wait response only) ===", pool.length);
    } else if ( _test_outgoing_requests > _test_outgoing_requests_max) {
      console.log("=== Pool length %d, beyond maximum test requests %d, no more submits (wait response only) ===", pool.length, _test_outgoing_requests);
    }
    clearInterval(timeout);
    _test_making_more = false;
  }
  var complete = true;
  for (var i = 0, len = pool.length; i < len; i++) {
    var inuse = pool[i].getInUse();
    if (inuse) {
      complete = false;
      console.log("Pool in use %d", i);
    } else {
      console.log("Pool available %d", i);
    }
  }
  if (!_test_making_more) {
    if (complete) {
      console.log("=== Pool length %d. Test success. requests=%d responses=%d ===", pool.length, _test_outgoing_requests, _test_incoming_response);
      process.exit();
    } else {
      console.log("=== Pool length %d, waiting for all to return to available (wait %d of %d) ===", pool.length, _test_wait_available_count, _test_wait_available_max);
    }
    _test_wait_available_count++;
    if (_test_wait_available_count > _test_wait_available_max) {
      console.log("=== Pool length %d. Test failed. requests=%d responses=%d ===", pool.length, _test_outgoing_requests, _test_incoming_response);

      // check connection still working
      console.log("*** check connection still working *** ");
      for (var i = 0, len = pool.length; i < len; i++) {
        var bconn = pool[i];
        var inuse = bconn.getInUse();
        if (inuse) {
          console.log("=== %d really dead (inuse=%d) === ",i, inuse);
          bconn.detach();
          inuse = bconn.getInUse();
          console.log("=== %d not anymore (inuse=%d) === ",i, inuse);
          try {
            bconn.stmt.execSync("SELECT TABLE_OWNER, TABLE_NAME FROM QSYS2.SYSTABLES where TABLE_NAME='BEAR'", function (result) { 
              bconn.detach();
              console.log("=== result " + JSON.stringify(result) + " ===");
            });
          } catch(ex) {
            bconn.detach();
            console.log("=== result exception ===");
          }
        }
      }

      process.exit();
    }
  }
}

function responseMe(res) {
  _test_incoming_response++;
  console.log("Response: %s", JSON.stringify(res) );
}


function requestMe(bb) {
  console.log("=== Submit %d requests requests=%d responses=%d ===", _test_outgoing_submit, _test_outgoing_requests, _test_incoming_response);
  _test_outgoing_requests += _test_outgoing_submit;
  for (var i = 0; i < _test_outgoing_submit; i++) { 
    bb.findById(_test_nbr, responseMe);
  }
}

bb = new bear.Bear();
// submit requests every second
var timeout = setInterval(requestMe, 1000, bb);
// check request/response matach every 5 seconds
bb.debugBearPool(function (pool) { var timein = setInterval(checkPool, 5000, pool); });
delete bb;

