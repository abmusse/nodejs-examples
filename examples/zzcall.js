/*
Author: Aaron Bartell
Descr.: Show how to call ZZCALL which is included with XMLSERVICE as an example program
*/

var xt = require('/QOpenSys/QIBM/ProdData/Node/os400/xstoolkit/lib/itoolkit')
var conn = new xt.iConn("*LOCAL")
var pgm = new xt.iPgm("ZZCALL", {"lib":"XMLSERVICE"})
pgm.addParam("x","1A")
pgm.addParam("x","1A")
pgm.addParam("777.7777", "7p4")
pgm.addParam("7777777777.77", "12p2")
pgm.addParam(
  [ 
    ["y","1A"],
    ["y","1A"],
    ["777.7777", "7p4"],
    ["7777777777.77", "12p2"]
])
conn.add(pgm.toXML())
function my_call_back(str) {
  var results = xt.xmlToJson(str)
  results.forEach(function(result, index){
    result.data.forEach(function(data, index2){
      console.log("type:" + data.type + " value:" + data.value)
    })
  })
}
conn.run(my_call_back)