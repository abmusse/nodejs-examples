module.exports = {
    database: "*LOCAL",
    user:     "KTURNER",
    password: "password",  
    options: {
        //host:     "174.79.32.155",
        //host:     "65.183.160.36",
        host:       "rnsdev",
        port:       8888,
        path:       "/cgi-bin/xmlcgi.pgm",
    },   
    dtaq:       "EVT_RNSSOK",
    dtaqlib:    "RNSROUTER",
    wait:       10,
    // Data queue has a buffer length of 64512 and a key length of 60
    bufflen:    64512,
    oper:       "EQ",
    key:        "A_60_char_key                                               "
}
 